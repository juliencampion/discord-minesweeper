Discord Minesweeper
===================

A Discord bot to play Minesweeper.

To run the bot, compile this Go package then run `discord-minesweeper`, then enter the bot token at the prompt.

The bot listens to all messages starting with `/mines`. Use `/mines help` for a list of available commands.
