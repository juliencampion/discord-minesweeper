package main

import (
	"fmt"
	"math/rand"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"

	"github.com/bwmarrin/discordgo"
)

const defaultWidth = 6
const defaultHeight = 6

const usage = "**Usage:** /mines [format]\n*Example: /mines 5x8*"

type game struct {
	width  int
	height int
	grid   [][]int
}

func (g game) String() string {
	ret := ""
	for _, row := range g.grid {
		line := ""
		for _, spot := range row {
			if line != "" {
				line += " "
			}
			line += "||"
			messages := []string{
				":bomb:",
				":zero:",
				":one:",
				":two:",
				":three:",
				":four:",
				":five:",
				":six:",
				":seven:",
				":eight:",
			}
			line += messages[spot+1]
			line += "||"
		}
		if ret != "" {
			ret += "\n"
		}
		ret += line
	}
	return ret
}

func newGame(width int, height int) game {
	g := game{}
	g.width = width
	g.height = height

	// Place mines
	for i := 0; i < g.height; i++ {
		row := []int{}
		for j := 0; j < g.width; j++ {
			val := 0
			if rand.Intn(5) == 0 {
				val = -1
			}
			row = append(row, val)
		}
		g.grid = append(g.grid, row)
	}

	// Update each field with the number of neighboring mines
	for i := 0; i < g.height; i++ {
		for j := 0; j < g.width; j++ {
			if g.grid[i][j] != -1 {
				for i2 := i - 1; i2 < i+2; i2++ {
					for j2 := j - 1; j2 < j+2; j2++ {
						if i2 >= 0 && i2 < g.height && j2 >= 0 && j2 < g.width {
							if g.grid[i2][j2] == -1 {
								g.grid[i][j]++
							}
						}
					}
				}
			}
		}
	}

	return g
}

func printError(s *discordgo.Session, m *discordgo.MessageCreate, message string) {
	s.ChannelMessageSend(m.ChannelID, "**Error**: "+message)
}

func printErrorUsage(s *discordgo.Session, m *discordgo.MessageCreate, message string) {
	s.ChannelMessageSend(m.ChannelID, "**Error**: "+message+"\n"+usage)
}

func main() {
	var token string
	fmt.Print("Bot token: ")
	fmt.Scanln(&token)

	dg, err := discordgo.New("Bot " + token)
	if err != nil {
		fmt.Println("Error creating Discord session: ", err)
		return
	}

	dg.AddHandler(messageCreate)

	err = dg.Open()
	if err != nil {
		fmt.Println("Error opening Discord session: ", err)
		return
	}

	fmt.Println("Discord Minesweeper is now running.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	dg.Close()
}

func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	words := strings.Fields(m.Content)
	if len(words) == 0 {
		return
	}

	if words[0] != "/mines" {
		return
	}

	if len(words) >= 2 && words[1] == "help" {
		s.ChannelMessageSend(m.ChannelID, usage)
		return
	}

	width := defaultWidth
	height := defaultHeight
	if len(words) >= 2 {
		elems := strings.Split(words[1], "x")
		if len(elems) != 2 {
			printErrorUsage(s, m, "size format not recognized")
			return
		}
		newWidth, newWidthErr := strconv.Atoi(elems[0])
		if newWidthErr != nil {
			printErrorUsage(s, m, "size format not recognized")
			return
		}
		newHeight, newHeightErr := strconv.Atoi(elems[1])
		if newHeightErr != nil {
			printErrorUsage(s, m, "size format not recognized")
			return
		}
		if newWidth < 1 {
			printError(s, m, "the grid must be at least 1 field wide")
			return
		}
		if newHeight < 1 {
			printError(s, m, "the grid must be at least 1 field high")
			return
		}
		if newWidth > 13 {
			printError(s, m, "the grid cannot be more than 13 fields wide (Discord message length limit)")
			return
		}
		if newHeight > 13 {
			printError(s, m, "the grid cannot be more than 13 fields high (Discord message length limit)")
			return
		}
		width = newWidth
		height = newHeight
	}

	g := newGame(width, height)
	message := fmt.Sprintf("%dx%d minesweeper grid:\n%s", g.width, g.height, g.String())
	s.ChannelMessageSend(m.ChannelID, message)
}
